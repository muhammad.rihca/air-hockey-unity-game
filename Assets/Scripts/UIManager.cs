﻿using UnityEngine;

public class UIManager : MonoBehaviour
{
    [Header("Canvas")]
    public GameObject CanvasGame;
    public GameObject CanvasRestart;

    [Header("CanvasRestart")]
    public GameObject WinText;
    public GameObject LoseText;

    [Header("Other")]
    public AudioManager audioManager;
    public ScoreScript scoreScript;
    public PuckScript puckScript;
    public PlayerMovement playerMovement;
    public AIScript aIScript;

    public void ShowRestartCanvas(bool didAIWin)
    {
        Time.timeScale = 0;

        CanvasGame.SetActive(false);
        CanvasRestart.SetActive(true);

        if (didAIWin)
        {
            audioManager.PlayLoseGame();
            WinText.SetActive(false);
            LoseText.SetActive(true);
        }
        else
        {
            audioManager.PlayWonGame();
            WinText.SetActive(true);
            LoseText.SetActive(false);
        }
    }

    public void RestartGame()
    {
        Time.timeScale = 1;

        CanvasGame.SetActive(true);
        CanvasRestart.SetActive(false);

        scoreScript.ResetScores();
        puckScript.CenterPuck();

        playerMovement.ResetPosition();
        aIScript.ResetPosition();
    }
}

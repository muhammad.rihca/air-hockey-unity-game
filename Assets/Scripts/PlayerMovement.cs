﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    bool wasJustClicked = true;
    bool canMove;
    // Vector2 playerSize;
    Rigidbody2D rb;
    Vector2 startingPosition;

    public Transform BoundaryHolder;

    Boundary playerBoundary;

    Collider2D playerCollider;

    // Start is called before the first frame update
    void Start()
    {
        // playerSize = GetComponent<SpriteRenderer>().bounds.extents;
        rb = GetComponent<Rigidbody2D>();
        startingPosition = rb.position;
        playerCollider = GetComponent<Collider2D>();

        playerBoundary = new Boundary(BoundaryHolder.GetChild(0).position.y,
        BoundaryHolder.GetChild(1).position.y,
        BoundaryHolder.GetChild(2).position.x,
        BoundaryHolder.GetChild(3).position.x);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (wasJustClicked)
            {
                wasJustClicked = false;

                // if ((mousePos.x >= transform.position.x && mousePos.x < transform.position.x + playerSize.x ||
                // mousePos.x <= transform.position.x && mousePos.x > transform.position.x - playerSize.x) &&
                // (mousePos.y >= transform.position.y && mousePos.y < transform.position.y + playerSize.y ||
                // mousePos.y <= transform.position.y && mousePos.y > transform.position.y - playerSize.y))
                if (playerCollider.OverlapPoint(mousePos))
                {
                    canMove = true;
                }
                else
                {
                    canMove = false;
                }
            }

            if (canMove)
            {
                // transform.position = mousePos;
                // rb.MovePosition(mousePos);

                Vector2 clampMousePos = new Vector2(Mathf.Clamp(mousePos.x, playerBoundary.Left, playerBoundary.Right),
                Mathf.Clamp(mousePos.y, playerBoundary.Down, playerBoundary.Up));

                rb.MovePosition(clampMousePos);
            }
        }
        else
        {
            wasJustClicked = true;
        }

    }

    public void ResetPosition()
    {
        rb.position = startingPosition;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public enum Score
    {
        AIScore, PlayerScore
    }

    public Text AIScoreText, PlayerScoreText;

    public UIManager uIManager;

    public int MaxScore;

    #region  Scores
    private int aiScore, playerScore;

    private int AIScore
    {
        get { return aiScore; }
        set
        {
            aiScore = value;
            if (value == MaxScore)
                uIManager.ShowRestartCanvas(true);
        }
    }

    private int PlayerScore
    {
        get { return playerScore; }
        set
        {
            playerScore = value;
            if (value == MaxScore)
                uIManager.ShowRestartCanvas(false);
        }
    }
    #endregion

    public void Increment(Score whichScore)
    {
        if (whichScore == Score.AIScore)
            // AIScoreText.text = (++aiScore).ToString();
            AIScoreText.text = (++AIScore).ToString();
        else
            // PlayerScoreText.text = (++playerScore).ToString();
            PlayerScoreText.text = (++PlayerScore).ToString();
    }

    public void ResetScores()
    {
        AIScore = PlayerScore = 0;
        AIScoreText.text = PlayerScoreText.text = "0";
    }
}
